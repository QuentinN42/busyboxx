"""
Download all videos in the product section of busyboxx.com
"""
from os import getenv, getcwd
from time import sleep

from selenium.webdriver import Firefox, FirefoxProfile


def get_user_pass() -> tuple:
    """
    Get user and password from environment variables.
    :return: (user, pass)
    """
    if getenv("USER") is not None and getenv("PASS") is not None:
        return getenv("USER"), getenv("PASS")
    raise EnvironmentError("You must set USER and PASS env vars.")


class Driver:
    """
    Main driver
    """

    def __init__(self):
        profile = FirefoxProfile()
        profile.set_preference("browser.download.folderList", 2)
        profile.set_preference("browser.download.manager.showWhenStarting", False)
        profile.set_preference("browser.download.dir", f"{getcwd()}/downloads")
        with open("files_types", "r") as types:
            profile.set_preference(
                "browser.helperApps.neverAsk.saveToDisk", types.read().replace("\n", ";")
            )
        self._driver = Firefox(executable_path="./geckodriver", firefox_profile=profile)
        self._driver.maximize_window()

    # Public methods

    def login(self):
        """ Log into the website """
        print("Logging in...")
        self._driver.get("https://www.busyboxx.com/Login")
        _user, _pass = get_user_pass()
        self._fill('//*[@id="EmailAddressTextBox"]', _user)
        self._fill('//*[@id="LoginPasswordTextBox"]', _pass)
        self._click('//*[@id="SignInButton"]')
        # Wait for the website to log you in...
        sleep(10)
        print("Logged in")

    # DL

    @property
    def todos(self):
        """ get all the pages to process """
        self._driver.get("https://www.busyboxx.com/EUR/Downloads")
        sleep(2)
        return self._driver.find_elements_by_class_name("contentsToDisplay")

    def download_on_this_page(self):
        """Download the videos

        Download the videos on this page with a sleep of 5 minutes at each 5 videos.

        - Get all videos
        - Split them 5 by 5
        - FOR each group of 5 videos
            - FOR each video in group
                - If popup open
                    - If "alpha" in option :
                        - Choice this one
                    - Else
                        - Choice the last
            - Sleep 5 minutes
        """
        sleep_time = 5 * 60
        all_videos = self._driver.find_elements_by_class_name("DownloadCloud")
        all_videos_by_five = [
            all_videos[k: k + 5] for k in range(0, len(all_videos), 5)
        ]
        nb_grps = len(all_videos_by_five)
    
        print(f"Total on this page : {len(all_videos)}.")
        print(f"    Estimated time : {nb_grps * sleep_time / 3600}h.")
    
        for i, group in enumerate(all_videos_by_five):
            print(f"Group {i} : {i / nb_grps * 100:.1f} % of this page.")
            for video in group:
                video.click()
                sleep(1)
                dl_options = self._driver.find_elements_by_class_name(
                    "BusyBoxinfoTextForContent"
                )
                if dl_options:
                    print(f"Got {len(dl_options)} options :")
                    all_option_to_text = "\n".join(
                        [o.text.replace("\n", "").lower() for o in dl_options]
                    )
                    print(all_option_to_text)
                    if all_option_to_text.count("alpha") == 0:
                        selected = dl_options[-1]
                        selected_txt = "Latest by default"
                    else:
                        potentials = [
                            i
                            for i in range(all_option_to_text.count("\n") + 1)
                            if "alpha" in all_option_to_text.split("\n")[i]
                        ]
                        selected = dl_options[potentials[-1]]
                        selected_txt = all_option_to_text.split("\n")[potentials[-1]]
                    print(f"Selected : {selected_txt}")
                    selected.click()
                else:
                    print("Only one choice.")
            sleep(sleep_time)

    def download(self):
        """ Download all the videos """
        print("Start download.")
        for i in range(len(self.todos)):
            todo = self.todos[i]
            print(todo.text)
            print(todo.location_once_scrolled_into_view)
            todo.click()
            sleep(2)
            self.download_on_this_page()
            print("---\n")
        print("Download successfully ended.")

    # Selenium functions

    def _fill(self, xpath, content):
        """ Fill xpath object with content """
        self._driver.find_element_by_xpath(xpath).send_keys(content)

    def _click(self, xpath):
        """ Click on xpath object """
        self._driver.find_element_by_xpath(xpath).click()


def main():
    """ Program entrypoint """
    driver = Driver()
    driver.login()
    driver.download()


if __name__ == "__main__":
    main()
