# Busyboxx

Auto download all busyboxx videos.

## Password

To use this script, pass user and password via env vars to the program. Copy the `.env.example` and fill it with your
personal information.

```shell
$ cp .env.example .env
```

## Dependencies 1

Install all dependencies and create output directory :

```shell
$ python3 -m pip install -r requirement.txt
$ mkdir downloads
```

## Dependencies 2

You also need [a webdriver](https://developer.mozilla.org/en-US/docs/Web/WebDriver) to run this program. Go to
the [releases section](https://github.com/mozilla/geckodriver/releases) and get the driver for your OS. Place it in this
folder by renaming it `geckodriver` (without extension).

## Run

Run the program :

```shell
$ source .env
$ python3 download.py
```
